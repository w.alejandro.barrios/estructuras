package usac.ipc1.Nodos;

import usac.ipc1.beans.Person;

public interface Pila {

    void apilar(Person person);

    void desapilar();

    Person obtener();

}
