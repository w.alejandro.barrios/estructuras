package usac.ipc1.Nodos;

import usac.ipc1.beans.Person;

public class ListaSimplementeEnlazada {

    private Nodo cabeza = null;
    private int longitud = 0;

    private class Nodo {
        public Person person;
        public Nodo siguiente = null;

        public Nodo(Person person) {
            this.person = person;
        }
    }

    public void insertarPrincipio(Person person) {
        Nodo nodo = new Nodo(person);
        nodo.siguiente = cabeza;
        cabeza = nodo;
        longitud++;

    }

    public void insertarFinal(Person person) {
        Nodo nodo = new Nodo(person);
        Nodo puntero = cabeza;
        while (puntero.siguiente != null) {
            puntero = puntero.siguiente;
        }
        puntero.siguiente = nodo;
        longitud++;
    }

    public void insertarMedio(int posicion, Person person) {
        Nodo nodo = new Nodo(person);
        if (cabeza == null) {
            cabeza = nodo;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            nodo.siguiente = puntero.siguiente;
            puntero.siguiente = nodo;
        }
        longitud++;
    }

    public Person buscar(int posicion) {
        if (cabeza == null) {
            return null;
        } else {
            Nodo puntero = cabeza;
            int contador = 0;
            while (contador < posicion && puntero.siguiente != null) {
                puntero = puntero.siguiente;
                contador++;
            }
            if (contador != posicion) {
                return null;
            } else {
                return puntero.person;
            }
        }
    }

    public int lengthLista() {
        return longitud;
    }

    public void eliminarPrincipio() {
        if (cabeza != null) {
            Nodo primero = cabeza;
            cabeza = cabeza.siguiente;
            primero.siguiente = null;
            longitud--;
        }
    }

    public void eliminarFinal() {
        if (cabeza != null) {
            if (cabeza.siguiente == null) {
                cabeza = null;
            } else {
                Nodo puntero = cabeza;
                while (puntero.siguiente.siguiente != null) {
                    puntero = puntero.siguiente;
                }
                puntero.siguiente = null;
            }
            longitud--;
        }
    }

    public void eliminarMedio(int posicion) {
        if (cabeza != null) {
            if (posicion == 0) {
                Nodo primero = cabeza;
                cabeza = cabeza.siguiente;
                primero.siguiente = null;
            } else if(posicion < longitud){
                Nodo puntero = cabeza;
                int contador = 0;
                while (contador < (posicion - 1)) {
                    puntero = puntero.siguiente;
                    contador++;
                }
                Nodo temporal = puntero.siguiente;
                puntero.siguiente = temporal.siguiente;
                temporal.siguiente = null;
            }
            longitud--;
        }
    }

}
