package usac.ipc1.Nodos;

import usac.ipc1.beans.Person;

public interface Cola {

    void insertar(Person person);

    void eliminar();

    Person procesar();

}
