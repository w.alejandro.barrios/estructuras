package usac.ipc1.Nodos;

import usac.ipc1.beans.Person;

public class ColaProceso implements Cola {

    private class Nodo {
        public Person person;
        public Nodo siguiente = null;

        public Nodo(Person person) {
            this.person = person;
        }
    }

    private Nodo cabeza, ultimo;

    @Override
    public void insertar(Person person) {
        Nodo nodo = new Nodo(person);
        if (cabeza == null) {
            cabeza = nodo;
        } else {
            ultimo.siguiente = nodo;
        }
        ultimo = nodo;
    }

    @Override
    public void eliminar() {
        if (cabeza != null){
            Nodo eliminar = cabeza;
            cabeza = cabeza.siguiente;
            eliminar.siguiente = null;
            if (cabeza == null){
                ultimo = null;
            }
        }
    }

    @Override
    public Person procesar() {
        if (cabeza == null) {
            return null;
        } else {
            return cabeza.person;
        }
    }
}
