package usac.ipc1.Nodos;

import usac.ipc1.beans.Person;

public class PilaProceso implements  Pila {

    private class Nodo{
        public Person person;
        public Nodo siguiente = null;

        public Nodo(Person person){
            this.person = person;
        }
    }

    private Nodo cima = null;

    @Override
    public void apilar(Person person) {
        Nodo nodo = new Nodo(person);
        nodo.siguiente = cima;
        cima = nodo;
    }

    @Override
    public void desapilar() {
        if (cima != null){
            Nodo eliminar = cima;
            cima = cima.siguiente;
            eliminar.siguiente = null;
        }
    }

    @Override
    public Person obtener() {
        if (cima == null){
            return null;
        }else{
            return cima.person;
        }
    }
}
